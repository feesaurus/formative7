import java.io.*;
import java.util.*;

public class Karyawan {
    public static void main(String[] args) {
        Gaji bulan1 = new Gaji("1-03-2022", 5_000_000, "monthly");
        Gaji bulan2 = new Gaji("1-04-2022", 5_000_000, "monthly");
        Gaji bulan3 = new Gaji("18-04-2022", 5_000_000, "thr");
        Gaji thr1 = new Gaji("1-05-2022", 5_000_000, "monthly");
        Gaji bulan4 = new Gaji("1-06-2022", 5_000_000, "monthly");
        Gaji bulan5 = new Gaji("1-07-2022", 5_000_000, "monthly");
        Gaji bulan6 = new Gaji("1-08-2022", 5_000_000, "monthly");
        Gaji bulan7 = new Gaji("1-09-2022", 5_000_000, "monthly");
        Gaji bulan8 = new Gaji("1-10-2022", 5_000_000, "monthly");
        Gaji bulan9 = new Gaji("1-11-2022", 5_000_000, "monthly");
        Gaji bulan10 = new Gaji("1-12-2022", 5_000_000, "monthly");

        ArrayList<Gaji> gajis = new ArrayList<Gaji>();

        gajis.add(bulan1);
        gajis.add(bulan2);
        gajis.add(thr1);
        gajis.add(bulan3);
        gajis.add(bulan4);
        gajis.add(bulan5);
        gajis.add(bulan6);
        gajis.add(bulan7);
        gajis.add(bulan8);
        gajis.add(bulan9);
        gajis.add(bulan10);

        System.out.println(gajis);

        try{
            FileOutputStream writeData = new FileOutputStream("./karyawan1.txt");
            ObjectOutputStream writeStream = new ObjectOutputStream(writeData);

            writeStream.writeObject(gajis);
            writeStream.flush();
            writeStream.close();

        }catch (IOException e) {
            e.printStackTrace();
        }
    }
}
