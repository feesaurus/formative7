public class Gaji {
    private String date;
    private int salary;
    private String typeSalary;

    public Gaji(String date, int salary, String typeSalary) {
        this.date=date;
        this.salary=salary;
        this.typeSalary=typeSalary;
    }

    public String toString() {
        return date + " " + salary + " " + typeSalary + "\n";
    }

    public String getDate() {
        return date;
    }

    public int getSalary() {
        return salary;
    }

    public String getTypeSalary() {
        return typeSalary;
    }
}
